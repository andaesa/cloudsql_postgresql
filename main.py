import datetime
import logging
import os
import socket
import requests

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

from google.cloud import pubsub


app = Flask(__name__)


def is_ipv6(addr):
    """Checks if a given address is an IPv6 address."""
    try:
        socket.inet_pton(socket.AF_INET6, addr)
        return True
    except socket.error:
        return False


# [START example]
# Environment variables are defined in app.yaml.
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

""" Create the table for the databases """
class Visit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime())
    user_ip = db.Column(db.String(46))
    latitude = db.Column(db.String(120))
    longitude = db.Column(db.String(120))

    def __init__(self, timestamp, user_ip, latitude, longitude):
        self.timestamp = timestamp
        self.user_ip = user_ip
        self.latitude = latitude
        self.longitude = longitude


@app.route('/')
def index():
    user_ip = request.remote_addr

    freegeoip = "http://freegeoip.net/json"
    geo_r = requests.get(freegeoip)
    geo_json = geo_r.json()

    # Keep only the first two octets of the IP address.
    if is_ipv6(user_ip):
        user_ip = ':'.join(user_ip.split(':')[:2])
    else:
        user_ip = '.'.join(user_ip.split('.')[:2])

    visit = Visit(
        user_ip=user_ip,
        timestamp=datetime.datetime.utcnow(),
        latitude=geo_json['latitude'],
        longitude=geo_json['longitude']
    )

    db.session.add(visit)
    db.session.commit()

    visits = Visit.query.order_by(sqlalchemy.desc(Visit.timestamp)).limit(10)

    results = [
        'Time: {} Addr: {} Latitude: {} Longitude {}'.format(x.timestamp, x.user_ip, x.latitude, x.longitude)
        for x in visits]

    output = 'Last 10 visits:\n{}'.format('\n'.join(results))

    def publish_message(topic_name, data):
        pubsub_client = pubsub.Client()
        topic = pubsub_client.topic("mytopic")

        data = results.encode('utf-8')
        message_id = topic.publish(data)


    return output, 200, {'Content-Type': 'text/plain; charset=utf-8'}
# [END example]


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)